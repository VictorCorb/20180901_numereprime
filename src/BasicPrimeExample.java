public class BasicPrimeExample {
    public static void main(String[] args) {
        int n = 7;
        int i = 2;
        boolean prime = true;

        while (i < (n / 2) && prime == true) {
            if(n % i == 0){
                prime = false;
            }
            i = i + 1;
        }
        if(prime == true){
            System.out.println("Number " + n + " is prime");
        } else {
            System.out.println("Number " + n + " is NOT prime");
        }
    }
}
